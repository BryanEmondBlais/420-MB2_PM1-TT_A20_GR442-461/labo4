const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    latitude: String,
    longitude: String,
    deg: String,
    hum: String,
    p: String,
    currentTime: String
});
module.exports = mongoose.model('data',dataSchema);