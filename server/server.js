const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const conn = mongoose.connection;

const User = require('./models/user');
const Data = require('./models/data');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

conn.once('open',()=>{
    console.log('connected to MongoDB');
});

app.post('/newUser',(req,res)=>{

    const user = new User(req.body);

    user.save((err,u)=>{
        if(err){
            return res.status(500).json(err);
        }
        res.status(201).json(u);
    })

})

app.post('/connection',(req,res)=>{
    console.log('req-body',req.body);
    User.find({"username":req.body.username,"password": req.body.password}).exec().then(u => res.status(200).json(u));
});

app.post('/newData',(req,res)=>{
    const data = new Data(req.body);

    data.save((err,d)=>{
        if(err){
            return res.status(500).json(err);
        }
        res.status(201).json(d);
    })
});


app.post('/readData',(req,res)=>{
    Data.find().exec().then(d => res.status(200).json(d));
})
mongoose.connect('mongodb://localhost:27017/android',{useUnifiedTopology: true,useNewUrlParser: true});

app.listen(3000,()=>{
    console.log('Listent Port 3000');
});