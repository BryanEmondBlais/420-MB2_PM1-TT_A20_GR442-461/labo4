package com.example.labo4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class dataArrayAdapter extends ArrayAdapter<dataClass> {
    private ArrayList<dataClass> ld;
    private Context context;
    private  int resource;
    public dataArrayAdapter(@NonNull Context context, int resource, @NonNull List<dataClass> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        ld = new ArrayList<dataClass>();
        ld = (ArrayList<dataClass>)objects;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        dataClass dataPos = this.ld.get(position);

        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        convertView = layoutInflater.inflate(this.resource,parent,false);
        TextView temp = (TextView)convertView.findViewById(R.id.itemNom);
        TextView date = (TextView)convertView.findViewById(R.id.ItemPrix);

        temp.setText("Temperature: "+dataPos.getDeg());
        date.setText("Current time: "+dataPos.getCurrentTime());
        return convertView;
    }
}