package com.example.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        Bundle ex = getIntent().getExtras();
        double temp = ex.getDouble("temp");
        termometre termometre = new termometre(this,temp);
        setContentView(termometre);
    }
}