package com.example.labo4;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import androidx.constraintlayout.solver.Cache;

public class termometre extends View {
    private int screenW,screenH;
    private Bitmap imageTher,resizedImage;
    private Paint rectPaint;
    private Context context;

    float tempGlobal;
    float moinQuarante;
    float plusCinquante;

    @Override
    protected  void onSizeChanged(int w, int h, int oldw, int oldh){
        super.onSizeChanged(w,h,oldw,oldh);
        screenH =h;
        screenW =w;
    }

    public termometre(Context context,double temp){
        super(context);
        this.context = context;
        this.imageTher = BitmapFactory.decodeResource(getResources(),R.drawable.thermo);
        rectPaint = new Paint();
        rectPaint.setAntiAlias(true);
        rectPaint.setColor(Color.BLACK);

        tempGlobal = 950;
        moinQuarante = (screenH/2)+720;
        plusCinquante = (screenH/2)-800;


        if(temp <= -40){
            tempGlobal = 1650;
        }
        else if(temp > -40 && temp < -30){
            tempGlobal = 1550;
        }
        else if(temp > -30 && temp < -20) {
            tempGlobal = 1350;
        }
        else if(temp > -20 && temp < -10){
            tempGlobal = 1200;
        }
        else if(temp > -10 && temp < -0){
            tempGlobal = 1050;
        }
        else if(temp >= 50) {
            tempGlobal = 100;
        }
        else if(temp <50 && temp > 40){
            tempGlobal = 200;
        }
        else if(temp <40 && temp > 30){
            tempGlobal = 350;
        }
        else if(temp <30 && temp > 20){
            tempGlobal = 550;
        }
        else if(temp <20 && temp > 10){
            tempGlobal = 700;
        }
        else if(temp <10 && temp > 0){
            tempGlobal = 850;
        }

        invalidate();

    }

    @Override
    protected void onDraw(Canvas canvas){

        resizedImage = Bitmap.createScaledBitmap(imageTher,screenW,screenH,true);
        canvas.drawBitmap(resizedImage,0,0,null);
        canvas.drawRect((screenW/2)-50,screenH,(screenW/2),tempGlobal,rectPaint);
    }

}