package com.example.labo4;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;

public class temperature implements SensorEventListener {

    private float deg;
    private SensorManager sensorManager;
    private Sensor temp;
    private Context context;

    public temperature(Context context){
        this.context = context;
        SensorManager sm = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
        List<Sensor> sensorList = sm.getSensorList(Sensor.TYPE_ALL);

        for(Sensor e:sensorList){
            System.out.println("Sensor vendor: " + e.toString());
            System.out.println("");
        }

        temp = sm.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sm.registerListener(this,temp,SensorManager.SENSOR_DELAY_NORMAL);
    }

                       @Override
    public void onSensorChanged(SensorEvent event) {
        deg = event.values[0];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public float getDeg() {
        return deg;
    }
}