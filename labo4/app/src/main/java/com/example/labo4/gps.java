package com.example.labo4;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class gps extends Service implements LocationListener {
    private static final int RUN_TIME_CODE = 0;
    private final Context context;

    private boolean isGpsEnabled = false;
    private boolean canGetLocation = false;

    private Location location;

    double latitude;
    double longitude;

    private static final long min_distance_change_for_updates = 10;
    private static final long min_time_bw_update = 1000 * 60 * 1;

    protected LocationManager locationManager;

    public gps(Context context) {
        this.context = context;
        getLocation();
    }


    private Location getLocation() {
        try {
            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!isGpsEnabled) {

            } else {
                this.canGetLocation = true;

                if (isGpsEnabled) {
                    if (location == null) {
                        if (ActivityCompat.checkSelfPermission((Activity)context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission((Activity)context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, min_time_bw_update, min_distance_change_for_updates, this);
                        }
                    }
                }
                if(locationManager != null){
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if(location != null){
                        latitude = location.getLatitude();
                        longitude =location.getLongitude();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e);
        }

        return location;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public boolean canGetLocation(){
        return this.canGetLocation;
    }
    public double getLatitude()
    {
        if(location != null)
        {
            latitude = location.getLatitude();

        }
        return latitude;
    }


    public double getLongitude()
    {
        if(location != null)
        {
            longitude = location.getLongitude();

        }
        return longitude;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    public void stopUsingGPS()
    {
        if(locationManager != null)
        {
            locationManager.removeUpdates(gps.this);

        }

    }

}