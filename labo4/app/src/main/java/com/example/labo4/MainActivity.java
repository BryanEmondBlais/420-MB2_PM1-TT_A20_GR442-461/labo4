package com.example.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText password;
    private EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        password = (EditText)findViewById(R.id.password);
        username = (EditText)findViewById(R.id.username);
    }

    public void onClick_conn(View view) {
        String userName = this.username.getText().toString();
        String pw = this.password.getText().toString();
        String cmd ="1";
        sendToNode send= new sendToNode(this);
        send.execute(cmd,userName,pw,view);

    }
}