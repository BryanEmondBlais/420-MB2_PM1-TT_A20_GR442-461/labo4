package com.example.labo4;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;

public class humidite implements SensorEventListener {

    private float hum;
    Context context;
    private Sensor humid;

    public humidite(Context context){
        this.context = context;
        SensorManager sm = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
        List<Sensor> sensorList = sm.getSensorList(Sensor.TYPE_ALL);

        for(Sensor e:sensorList){
            System.out.println("Sensor vendor: " + e.toString());
            System.out.println("");
        }

        humid = sm.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sm.registerListener(this,humid,SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        hum = event.values[0];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public float getHum() {
        return hum;
    }
}