package com.example.labo4;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;

public class MainActivity3 extends AppCompatActivity {
    List<dataClass> dataList;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        listView = (ListView)findViewById(R.id.listView);
        final dataArrayAdapter adapters = new dataArrayAdapter(this,R.layout.listview,dataList);


        Bundle ex = getIntent().getExtras();
        dataList = new ArrayList<dataClass>();
        try{
            String data = ex.getString("data");
            data = data.replace(":",",");
            data = data.replace(",{","");
            data = data.replace("{","");
            data = data.replace("[","");
            data = data.replace("]","");
            data = data.replace(",\"__v\",0","");
            data = data.replace("\"","");

            String dataArray[] = data.split(Pattern.quote("}"));

            for(String e:dataArray){
                String array[] = e.split(",",14);
                dataList.add(new dataClass(array[1],array[13],Double.parseDouble(array[3]),Double.parseDouble(array[5]),Double.parseDouble(array[9]),Double.parseDouble(array[7]),Double.parseDouble(array[11])));
            }

            dataArrayAdapter adapter = new dataArrayAdapter(this,R.layout.listview,dataList);
            listView.setAdapter(adapter);

            int i =0;
        }catch (Exception e){
            System.out.println(e);
        }

        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(MainActivity3.this,MainActivity4.class);
                i.putExtra("temp",dataList.get(position).getDeg());
                MainActivity3.this.startActivity(i);
            }
        });
    }
}