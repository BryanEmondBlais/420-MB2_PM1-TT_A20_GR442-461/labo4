package com.example.labo4;

public class dataClass {
    private String id,currentTime;
    private double latitude,longitude,hum,deg,pressure;
    public dataClass(){

    }

    public dataClass(String id, String currentTime, double latitude, double longitude, double hum, double deg, double pressure) {
        this.id = id;
        this.currentTime = currentTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.hum = hum;
        this.deg = deg;
        this.pressure = pressure;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getHum() {
        return hum;
    }

    public void setHum(double hum) {
        this.hum = hum;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }
}