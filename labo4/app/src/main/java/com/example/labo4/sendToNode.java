package com.example.labo4;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;


public class sendToNode extends AsyncTask {

    private Context context;
    private URL url;
    public String nom;
    public String pw;
    BufferedWriter bufw;
    OutputStream outs;
    HttpURLConnection conn;


    public sendToNode(Context context){
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        String text;
        switch ((String)objects[0]){
            case "1":
                String lien = "http://10.0.0.202:3000/connection";
                try{
                    url = new URL(lien);
                    conn  = (HttpURLConnection)url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    outs = conn.getOutputStream();
                    bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                    String msg = URLEncoder.encode("username","utf-8")+"="+
                                URLEncoder.encode((String)objects[1],"utf8")+"&"+
                                URLEncoder.encode("password","utf-8")+"="+
                                URLEncoder.encode((String)objects[2],"utf8");
                    bufw.write(msg);
                    bufw.flush();
                    bufw.close();
                    bufw.close();

                    InputStream ins = conn.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();

                    while ((line=bufr.readLine())!=null){
                        sbuff.append(line);
                    }
                    text = sbuff.toString().trim();
                    if (text != null) {
                        Intent i = new Intent(context,MainActivity2.class);
                        context.startActivity(i);
                    }
                }catch (Exception e){
                    System.out.println(e);
                }
                break;
            case "2":
                String lien2 = "http://10.0.0.202:3000/newData";
                try{
                    url = new URL(lien2);
                    conn  = (HttpURLConnection)url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    outs = conn.getOutputStream();
                    bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                    String msg = URLEncoder.encode("latitude","utf-8")+"="+
                            URLEncoder.encode((String)objects[1],"utf8")+"&"+
                            URLEncoder.encode("longitude","utf-8")+"="+
                            URLEncoder.encode((String)objects[2],"utf8")+"&"+
                            URLEncoder.encode("deg","utf-8")+"="+
                            URLEncoder.encode((String)objects[3],"utf8")+"&"+
                            URLEncoder.encode("hum","utf-8")+"="+
                            URLEncoder.encode((String)objects[4],"utf8")+"&"+
                            URLEncoder.encode("p","utf-8")+"="+
                            URLEncoder.encode((String)objects[5],"utf8")+"&"+
                            URLEncoder.encode("currentTime","utf-8")+"="+
                            URLEncoder.encode((String)objects[6],"utf8");
                    bufw.write(msg);
                    bufw.flush();
                    bufw.close();
                    bufw.close();

                    InputStream ins = conn.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();
                    while ((line=bufr.readLine())!=null){
                        sbuff.append(line);
                    }
                    text = sbuff.toString();

                }catch (Exception e){
                    System.out.println(e);
                }
                break;
            case "3":
                String lien3 = "http://10.0.0.202:3000/readData";
                try{
                    url = new URL(lien3);
                    conn  = (HttpURLConnection)url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    InputStream ins = conn.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();
                    while ((line=bufr.readLine())!=null){
                        sbuff.append(line);
                    }
                    text = sbuff.toString();

                    Intent i = new Intent(context,MainActivity3.class);
                    i.putExtra("data",text);
                    context.startActivity(i);


                }catch (Exception e){
                    System.out.println(e);
                }
                break;

        }

        return null;
    }

}