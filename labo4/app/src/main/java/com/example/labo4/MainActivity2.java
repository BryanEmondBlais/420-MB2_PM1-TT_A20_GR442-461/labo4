package com.example.labo4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.os.Bundle;
import android.view.View;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {

    gps gps;
    temperature temperature;
    humidite humidite;
    pression pression;

    double latitude;
    double longitude;

    private static final int RUN_TIME_CODE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        temperature = new temperature(this);
        humidite = new humidite(this);
        pression = new pression(this);
        if (isGPSAllowed()) {
            return;
        }
        requestPermissionGPS();


    }


    public void onClick_vis(View view) {
        String cmd ="3";
        sendToNode send= new sendToNode(this);
        send.execute(cmd,view);

    }

    private void requestPermissionGPS()
    {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            showAlertDialog();
        }
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},RUN_TIME_CODE);
    }
    private void showAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Runtime Permission");
        alertDialogBuilder.setMessage("Cette application a besoin d'accéder aux:"+"GPS et  données cellulaire."+"Donner les permissions.").setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private boolean isGPSAllowed()
    {
        int resultGps = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(resultGps == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void onClick_add(View view) {
        gps = new gps(this);
        if(gps.canGetLocation()){
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }
        String currentTime = Calendar.getInstance().getTime().toString();
        float deg = temperature.getDeg();
        float hum = humidite.getHum();
        float p = pression.getP();
        String cmd ="2";
        sendToNode send= new sendToNode(this);
        send.execute(cmd,Double.toString(latitude),Double.toString(longitude),Double.toString(deg),Double.toString(hum),Double.toString(p),currentTime,view);
    }
}