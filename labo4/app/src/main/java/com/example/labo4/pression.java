package com.example.labo4;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;

public class pression implements SensorEventListener {
    private float p;
    private SensorManager sensorManager;
    private Sensor pres;
    private Context context;

    public pression(Context context){
        this.context = context;
        SensorManager sm = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);
        List<Sensor> sensorList = sm.getSensorList(Sensor.TYPE_ALL);

        for(Sensor e:sensorList){
            System.out.println("Sensor vendor: " + e.toString());
            System.out.println("");
        }

        pres = sm.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sm.registerListener(this,pres,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        p = event.values[0];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public float getP() {
        return p;
    }
}